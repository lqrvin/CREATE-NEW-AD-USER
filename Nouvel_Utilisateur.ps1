﻿# ================================================================== #
#           SCRIPT DE CREATION D'UTILISATEUR DANS L'AD
# ================================================================= #

Import-Module ActiveDirectory



# ============================================ #
#               LES FONCTIONS
# ============================================ #

Function password {

Param(
    [int]$Lenght
)


if(!($Lenght)){
    $Lenght=7
}

$pass=""

for ($i=0;$i -lt $Lenght;$i++){
    $pass += Get-Random -InputObject a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,1,2,3,4,5,6,7,8,9
}

$spe = Get-Random -InputObject !,?,.,*,:,-,_,$,=,`+,`&

$pass = $pass.Insert((Get-Random -Minimum 7 -Maximum ($pass.Length + 1)), $spe)
return $pass
}
Function clipboard($clipboard) {

    Add-Type -AssemblyName System.Windows.Forms

    $tb = New-Object System.Windows.Forms.TextBox
    $tb.Multiline = $true
    $tb.Text = $clipboard
    $tb.SelectAll()
    $tb.Copy()
}
Function checkuser {

    $prenomCheck = $prenomBox.Text.Trim()
    $nomCheck = $nomBox.Text.Trim()

    $sessionCheck = "$prenomCheck.$nomCheck".ToLower()

        if (@(Get-ADUser -Filter {SamAccountName -eq $sessionCheck}).Count -eq 1) {

            Write-Warning -Message "$sessionCheck existe déjà"

            $textuserexist.Location = New-Object System.Drawing.Point(130, 300)
            $textuserexist.Size = New-Object System.Drawing.Size(210, 20)
            $textuserexist.Text = "ATTENTION : Cet utilisateur existe déjà !"
            #$form.Controls.Add($textuserexist)

        }

        else {

            Write-Warning -Message "Cet utilisateur n'exsite pas, il peut être crée"

            $textuserexist.Location = New-Object System.Drawing.Point(160, 300)
            $textuserexist.Size = New-Object System.Drawing.Size(139, 20)
            $textuserexist.Text = "Cet utilisateur n'existe pas."
  
        }
}
Function createuser{

        # Création de l'utilisateur

            # Si DateYes + GroupeYes
            If ($a -ne $b -and -not [string]::IsNullOrEmpty($tse)) { 

                New-ADUser -AccountExpirationDate $date -DisplayName $nomfull -Name $nomfull -GivenName $prenom -Surname $nom -SamAccountName $session -UserPrincipalName $sessionfull -Path "OU=$OU,DC=mt,DC=rivagroup,DC=local" -AccountPassword (ConvertTo-SecureString -AsPlainText "Riva100!" -Force) -Enabled $true -Description $description
                Start-Sleep 1
                Add-ADGroupMember -Identity $tse -Members $session
            }

            # Si DateYes + GroupeNo
            elseIf ($a -ne $b -and [string]::IsNullOrEmpty($tse)) {

                New-ADUser -AccountExpirationDate $date -DisplayName $nomfull -Name $nomfull -GivenName $prenom -Surname $nom -SamAccountName $session -UserPrincipalName $sessionfull -Path "OU=$OU,DC=mt,DC=rivagroup,DC=local" -AccountPassword (ConvertTo-SecureString -AsPlainText "Riva100!" -Force) -Enabled $true -Description $description
            }

            # Si DateNo + GroupeYes
            elseIf ($a -eq $b -and -not [string]::IsNullOrEmpty($tse)) {

                New-ADUser -DisplayName $nomfull -Name $nomfull -GivenName $prenom -Surname $nom -SamAccountName $session -UserPrincipalName $sessionfull -Path "OU=$OU,DC=mt,DC=rivagroup,DC=local" -AccountPassword (ConvertTo-SecureString -AsPlainText "Riva100!" -Force) -Enabled $true -Description $description
                Start-Sleep 1
                Add-ADGroupMember -Identity $tse -Members $session
            }

            # Si DateNo + GroupeNo
            elseIf ($a -eq $b -and [string]::IsNullOrEmpty($tse)) {

                New-ADUser -DisplayName $nomfull -Name $nomfull -GivenName $prenom -Surname $nom -SamAccountName $session -UserPrincipalName $sessionfull -Path "OU=$OU,DC=mt,DC=rivagroup,DC=local" -AccountPassword (ConvertTo-SecureString -AsPlainText "Riva100!" -Force) -Enabled $true -Description $description
            }


}

# ======================================================= #
#            FENÊTRE DE DEMANDE DE SAISIE :
# ======================================================= #
    
    Add-Type -AssemblyName System.Windows.Forms
    Add-Type -AssemblyName System.Drawing

    $form = New-Object System.Windows.Forms.Form
    $form.Text = 'Nouvel Utilisateur'
    $form.Size = New-Object System.Drawing.Size(470,410)
    $form.StartPosition = 'CenterScreen'
    
    $okButton = New-Object System.Windows.Forms.Button
    $okButton.Location = New-Object System.Drawing.Point(363, 330)
    $okButton.Size = New-Object System.Drawing.Size(75, 23)
    $okButton.Text = 'Valider'
    $okButton.DialogResult = [System.Windows.Forms.DialogResult]::OK
    $form.AcceptButton = $okButton
    $form.Controls.Add($okButton)
    
    $cancelButton = New-Object System.Windows.Forms.Button
    $cancelButton.Location = New-Object System.Drawing.Point(20, 330)
    $cancelButton.Size = New-Object System.Drawing.Size(75, 23)
    $cancelButton.Text = 'Annuler'
    $cancelButton.DialogResult = [System.Windows.Forms.DialogResult]::Cancel
    $form.CancelButton = $cancelButton
    $form.Controls.Add($cancelButton)

    $checkButton = New-Object System.Windows.Forms.Button
    $checkButton.Location = New-Object System.Drawing.Point(190, 330)
    $checkButton.Size = New-Object System.Drawing.Size(75, 23)
    $checkButton.Text = 'Tester'
    $checkButton.Add_Click({checkuser})
    $form.Controls.Add($checkButton)

# ======================================================= #
#                       Prénom
# ======================================================= #
 
    $prenomtxt = New-Object System.Windows.Forms.Label
    $prenomtxt.Location = New-Object System.Drawing.Point(12, 84)
    $prenomtxt.Size = New-Object System.Drawing.Size(55, 15)
    $prenomtxt.Text = 'Prénom :'
    $form.Controls.Add($prenomtxt)
    
    $prenomBox = New-Object System.Windows.Forms.TextBox
    $prenomBox.Location = New-Object System.Drawing.Point(102, 81)
    $prenomBox.Size = New-Object System.Drawing.Size(134, 23)
    $form.Controls.Add($prenomBox)

# ======================================================= #
#                         Nom
# ======================================================= #

    $nomtxt = New-Object System.Windows.Forms.Label
    $nomtxt.Location = New-Object System.Drawing.Point(258, 84)
    $nomtxt.Size = New-Object System.Drawing.Size(40, 15)
    $nomtxt.Text = 'Nom :'
    $form.Controls.Add($nomtxt)
    
    $nomBox = New-Object System.Windows.Forms.TextBox
    $nomBox.Location = New-Object System.Drawing.Point(304, 81)
    $nomBox.Size = New-Object System.Drawing.Size(134, 23)
    $form.Controls.Add($nomBox)
   
# ======================================================= #
#                     Description
# ======================================================= #

    $descriptiontxt = New-Object System.Windows.Forms.Label
    $descriptiontxt.Location = New-Object System.Drawing.Point(12, 114)
    $descriptiontxt.Size = New-Object System.Drawing.Size(73, 15)
    $descriptiontxt.Text = 'Description :'
    $form.Controls.Add($descriptiontxt)

    $descriptionBox = New-Object System.Windows.Forms.TextBox
    $descriptionBox.Location = New-Object System.Drawing.Point(102, 111)
    $descriptionBox.Size = New-Object System.Drawing.Size(336, 23)
    $form.Controls.Add($descriptionBox)

# ======================================================= #
#                          OU
# ======================================================= #

    $outxt = New-Object System.Windows.Forms.Label
    $outxt.Location = New-Object System.Drawing.Point(12, 143)
    $outxt.Size = New-Object System.Drawing.Size(30, 15)
    $outxt.Text = 'OU :'
    $form.Controls.Add($outxt)

    $ouBox = New-Object System.Windows.Forms.Combobox
    $ouBox.Location = New-Object Drawing.Point (102, 140)
    $ouBox.Size = New-Object System.Drawing.Size(127, 23)
    $ouBox.DropDownStyle = "DropDownList"
    $ouBox.Items.AddRange(("test","__Sharepoint","_Guardian","_SERVICE_INFO","_Temp_Workers","TSE"))
    $ouBox.SelectedIndex = 0
    $form.controls.add($ouBox)

# ======================================================= #
#                    Date d'expiration
# ======================================================= #

    $datetxt = New-Object System.Windows.Forms.Label
    $datetxt.Location = New-Object System.Drawing.Point(235, 143)
    $datetxt.Size = New-Object System.Drawing.Size(103, 15)
    $datetxt.Text = "Date d'expiration :"
    $form.Controls.Add($datetxt)

    $datepicker = New-Object System.Windows.Forms.DateTimePicker
    $datepicker.Location = New-Object System.Drawing.Point(344, 140)
    $datepicker.Size = New-Object System.Drawing.Size(94, 23)
    $datepicker.Format = "Short"
    $datetoday = Get-Date -Format “MM/dd/yyyy”
    $datepicker.MinDate = "$datetoday"
    $form.Controls.Add($datepicker)

# ======================================================= #
#                     Membre de
# ======================================================= #

    $tsetxt = New-Object System.Windows.Forms.Label
    $tsetxt.Location = New-Object System.Drawing.Point(15, 230)
    $tsetxt.Size = New-Object System.Drawing.Size(74, 15)
    $tsetxt.Text = "Membre de :"
    $form.Controls.Add($tsetxt)

    $tseBox = New-Object System.Windows.Forms.ListBox
    $tseBox.Location = New-Object System.Drawing.Point(100, 220)
    $tseBox.Size = New-Object System.Drawing.Size(265, 79)
    $form.Controls.Add($tseBox)

    $tseBox.Items.Add('TSE_1007')
    $tseBox.Items.Add('TSE_1013')
    $tseBox.Items.Add('TSE_1007_GENERIQUE')
    $tseBox.Items.Add('TSE_1013_GENERIQUE')
    $tseBox.Items.Add('')

# ======================================================= #
#                    Autres texte
# ======================================================= #

    $image = New-Object System.Windows.Forms.pictureBox
    $image.Location = New-Object Drawing.Point 12,12
    $image.Size = New-Object System.Drawing.Size(25,33)
    $image.image = [system.drawing.image]::FromFile("C:\Scripts\AD\CREATE_NEW_AD-USER\usericon.png")
    $form.Controls.Add($image)

    $text1 = New-Object System.Windows.Forms.Label
    $text1.Location = New-Object System.Drawing.Point(73, 24)
    $text1.Size = New-Object System.Drawing.Size(69, 15)
    $text1.Text = 'Créer dans :'
    $form.Controls.Add($text1)

    $text2 = New-Object System.Windows.Forms.Label
    $text2.Location = New-Object System.Drawing.Point(148, 24)
    $text2.Size = New-Object System.Drawing.Size(132, 15)
    $text2.Text = "mt.rivagroup.local/"
    $form.Controls.Add($text2)

    $trait1 = New-Object System.Windows.Forms.Label
    $trait1.Location = New-Object System.Drawing.Point(33, 48)
    $trait1.Size = New-Object System.Drawing.Size(387, 15)
    $trait1.Text = '_____________________________________________________________________________________'
    $form.Controls.Add($trait1)

    $trait2 = New-Object System.Windows.Forms.Label
    $trait2.Location = New-Object System.Drawing.Point(32, 170)
    $trait2.Size = New-Object System.Drawing.Size(387, 15)
    $trait2.Text = '_____________________________________________________________________________________'
    $form.Controls.Add($trait2)

    $textuserexist = New-Object System.Windows.Forms.Label
    $form.Controls.Add($textuserexist)

# ======================================================= #
#              Enregistrement des Résultats
# ======================================================= #

$result = $form.ShowDialog()


# Si on clique sur OK alors on enregistre les valeurs 
if ($result -eq [System.Windows.Forms.DialogResult]::OK) {
    $prenomBrut = $prenomBox.Text.Trim()
    $nomBrut = $nomBox.Text.Trim()
    $description = $descriptionBox.Text
    $ou = $ouBox.Text
    $date = $datepicker.Text
    $tse = $tseBox.Text
} 

# Si on clique sur Annuler alors cela quitte le programme
else {
    [Environment]::Exit(1) 
}



# ============================================ #
#                LE PROGRAMME
# ============================================ #

        # Génération du mot de passe
        $password = password
        

        # Mise en forme du nom et du prenom
        $prenom = (Get-Culture).textinfo.totitlecase($prenomBrut)
        $nom = $nomBrut.ToUpper()


        # Nom d'ouverture de sessions de l'utilisateur
        $session = "$prenom.$nom".ToLower()
        
		# Nom d'ouverture de sessoin avec @rivagroup.com
        $sessionfull = "$prenom.$nom@rivagroup.com".ToLower()
		
		
        # Nom complet
        $nomfull = "$nom $prenom"
        
        
        # Copie dans le Presse-Papier le nom d'ouverture de sessions ainsi que le mot de passe généré de l'utilisateur pour le coller dans la Fiche Parcours Embauche
        $clipboard = "$session Riva100!"
        clipboard($clipboard)


        # Enregistrement dans le fichier texte le nom d'ouverture de sessions ainsi que le mot de passe généré de l'utilisateur
        $savetxt = "$session = $password"
        $pathtxt = "C:\Scripts\AD\CREATE_NEW_AD-USER\Historique.txt"
        $GetContentLog = Get-Content -Path $pathtxt
        $GetContentLog[0] = "{0}`r`n{1}" -f $savetxt, $GetContentLog[0]
        $GetContentLog | Set-Content $pathtxt


        # Génération de la date du jour
        $date_now = Get-Date -DisplayHint Date
        $date = (Get-Date $date).ToLongDateString()

        # Convertion des dates au même format
        $a = (Get-Date $date).ToShortDateString()
        $b = (Get-Date $date_now).ToShortDateString()

       

        # Création de l'utilisateur
        createuser


       


# ---------------------------------------------------------------------------------------------------------------------------------
# source : https://docs.microsoft.com/fr-fr/powershell/scripting/samples/creating-a-custom-input-box?view=powershell-7.1
# source : https://docs.microsoft.com/fr-fr/powershell/scripting/samples/selecting-items-from-a-list-box?view=powershell-7.1
# source : https://docs.microsoft.com/fr-fr/powershell/scripting/samples/creating-a-graphical-date-picker?view=powershell-7.1
# source : https://docs.microsoft.com/en-us/dotnet/desktop/winforms/?view=netframeworkdesktop-4.8
# source : https://powershell.sekki.fr/2012/11/liste-deroulante-et-table-de-donnees.html
# source : https://macintom.com/wp/2012/07/03/powershell-ajouter-une-ligne-dans-un-fichier-texte/
# source : https://www.synergeek.fr/powershell-placer-du-texte-dans-le-presse-papier/
# source : http://www.blog4me.fr/article293/powershell-insertion-de-message-sur-la-premiere-ligne-d-un-fichier-texte
# source : https://itpro-tips.com/2018/powershell-test-ad-object-exists/
# source : https://www.journaldunet.fr/web-tech/developpement/1441221-comment-verifier-qu-une-chaine-de-caracteres-string-n-est-pas-nulle-null-ou-vide-empty-en-powershell/
# source : http://www.delahaye.fr/conversion-de-texte-majuscules-minuscules/
# source : https://www.it-connect.fr/powershell-structure-conditionnelle-if-else-et-elseif/
# ---------------------------------------------------------------------------------------------------------------------------------